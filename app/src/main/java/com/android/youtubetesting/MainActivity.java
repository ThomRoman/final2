package com.android.youtubetesting;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;


public class MainActivity extends AppCompatActivity {
    TextView tv_date;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Instanciamos el objeto Calendar
        //en fecha obtenemos la fecha y hora del sistema
        Calendar fecha = new GregorianCalendar();

        //Obtenemos el valor del año, mes, día,
        //hora, minuto y segundo del sistema
        //usando el método get y el parámetro correspondiente
        int year = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        //Mostramos por pantalla dia/mes/año
        String fechaString = "Fecha Actual: " + dia + "/" + (mes+1) + "/" + year;
        tv_date = findViewById(R.id.tv_date);

        tv_date.setText(fechaString);
    }
    // para mostrar o ocultar el overflow del actionbar
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.overflow,menu);
        return true;
    }

    // metodo para asignar las funciones correspondientes a las opciones
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        Intent intent= null;
        switch (id){
            case R.id.idCarreras:
                intent = new Intent (this, CarrerasUntels.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Toast.makeText(this,"Carreras y lugares principales",Toast.LENGTH_LONG).show();
                break;
            case R.id.idHome :
                intent = new Intent (this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Toast.makeText(this,"Pagina principal",Toast.LENGTH_LONG).show();
                break;
            case R.id.idDashboard:
                intent = new Intent (this, Dashboard.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Toast.makeText(this,"Dashboard",Toast.LENGTH_LONG).show();
                break;
        }
        startActivity(intent);
        finish();
        return super.onOptionsItemSelected(item);
    }


}
