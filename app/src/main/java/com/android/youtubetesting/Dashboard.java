package com.android.youtubetesting;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class Dashboard extends AppCompatActivity implements OnMapReadyCallback {
    private MapView mMapView;
    PieChart pastel;
    BarChart barra;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        pastel = findViewById(R.id.graficoPastel);
        barra = findViewById(R.id.graficoBarras);
        
        crearGraficoBarra();
        crearGraficoPastel();

        mMapView = (MapView) findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.getMapAsync(this);
        
    }

    private void crearGraficoBarra() {
        Description description = new Description();
        description.setText("Grafico de Barras");
        description.setTextSize(13);
        barra.setFitBars(true);
        List<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,10));
        barEntries.add(new BarEntry(2,15));
        barEntries.add(new BarEntry(3,5));
        barEntries.add(new BarEntry(4,2));
        BarDataSet barDataSet = new BarDataSet(barEntries,"Data set");
        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        barDataSet.setDrawValues(true);

        BarData data = new BarData(barDataSet);
        barra.setData(data);
        barra.invalidate();
        barra.animate();
    }

    private void crearGraficoPastel() {
        Description description = new Description();
        description.setText("Grafico de pastel");
        description.setTextSize(13);

        pastel.setExtraOffsets(5,10,5,5);
        pastel.setUsePercentValues(true);
        pastel.setDragDecelerationFrictionCoef(0.95f);
        pastel.setDrawHoleEnabled(true);
        pastel.setHoleColor(Color.WHITE);
        pastel.setTransparentCircleRadius(61f);
        pastel.setDescription(description);
        List<PieEntry> pieEntries = new ArrayList<>();

        pieEntries.add(new PieEntry(34f,"PartyA"));
        pieEntries.add(new PieEntry(23f,"USA"));
        pieEntries.add(new PieEntry(14f,"UK"));
        pieEntries.add(new PieEntry(35,"India"));

        PieDataSet pieDataSet = new PieDataSet(pieEntries,"Contries");
        pieDataSet.setSliceSpace(3f);
        pieDataSet.setSelectionShift(5f);
        pieDataSet.setColors(ColorTemplate.JOYFUL_COLORS);

        PieData pieData = new PieData(pieDataSet);
        pieData.setValueTextSize(15f);
        pastel.setData(pieData);
    }


    // para mostrar o ocultar el overflow del actionbar
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.overflow,menu);
        return true;
    }

    // metodo para asignar las funciones correspondientes a las opciones
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        Intent intent= null;
        switch (id){
            case R.id.idCarreras:
                intent = new Intent (this, CarrerasUntels.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Toast.makeText(this,"Carreras y lugares principales",Toast.LENGTH_LONG).show();
                break;
            case R.id.idHome :
                intent = new Intent (this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Toast.makeText(this,"Pagina principal",Toast.LENGTH_LONG).show();
                break;
            case R.id.idDashboard:
                intent = new Intent (this, Dashboard.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Toast.makeText(this,"Dashboard",Toast.LENGTH_LONG).show();
                break;
        }
        startActivity(intent);
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        float zoomLevel = 16.0f;
        final LatLng position = new LatLng(-12.2142495,-76.9324986);
        map.addMarker(new MarkerOptions().position(position).title("UNTELS"));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(position,zoomLevel));
//        map.moveCamera(CameraUpdateFactory.newLatLng(position));
        map.setMapType(GoogleMap.MAP_TYPE_HYBRID);

    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);

    }
}