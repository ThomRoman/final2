package com.android.youtubetesting;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ImageListener;

public class CarrerasUntels extends AppCompatActivity {
    CarouselView carouselView;

    int[] sampleImages = {
            R.drawable.admin,
            R.drawable.sistemas,
            R.drawable.ambien,
            R.drawable.tele,
            R.drawable.electrica,
            R.drawable.biblioteca,
            R.drawable.campus
    };
    String[] mImageTitle = {
            "Administracion",
            "Ingenieria de sistemas",
            "Ingenieria Ambiental",
            "Ingenieria de Telecomunicaciones",
            "Ingenieria Electrica",
            "Biblioteca",
            "Campus completo"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carreras_untels);

        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleImages.length);

        carouselView.setImageListener(imageListener);
        carouselView.setImageClickListener(new ImageClickListener() {
            @Override
            public void onClick(int position) {
                Toast.makeText(getApplicationContext(),mImageTitle[position],Toast.LENGTH_LONG).show();
            }
        });
    }
    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };

    // para mostrar o ocultar el overflow del actionbar
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.overflow,menu);
        return true;
    }

    // metodo para asignar las funciones correspondientes a las opciones
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        Intent intent= null;
        switch (id){
            case R.id.idCarreras:
                intent = new Intent (this, CarrerasUntels.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Toast.makeText(this,"Carreras y lugares principales",Toast.LENGTH_LONG).show();
                break;
            case R.id.idHome :
                intent = new Intent (this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Toast.makeText(this,"Pagina principal",Toast.LENGTH_LONG).show();
                break;
            case R.id.idDashboard:
                intent = new Intent (this, Dashboard.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                Toast.makeText(this,"Dashboard",Toast.LENGTH_LONG).show();
                break;
        }
        startActivity(intent);
        finish();
        return super.onOptionsItemSelected(item);
    }
}